const { test, expect } = require('@playwright/test');
const { newApp } = require('./util');

test.describe('Feature tests', function () {
  let electron;
  let window;
  test.beforeEach(async function () {
    electron = await newApp({
      loggedIn: true,
    });
    window = electron.window;
  });

  test('shows overview page', async function () {
    const issues = window.locator('#issues-count');
    const mrs = window.locator('#mrs-count');
    const todos = window.locator('#todos-count');

    await window.screenshot({
      path: 'test-results/screenshots/features/shows-overview-page.png',
      fullPage: true,
    });
    expect(await issues.count()).toBe(1);
    expect(await mrs.count()).toBe(1);
    expect(await todos.count()).toBe(1);
  });
});

const { test, expect } = require('@playwright/test');
const { newApp } = require('./util');

test.describe('Application launch', function () {
  let electron;
  test.beforeEach(async function () {
    electron = await newApp();
  });

  test('starts the menubar window', async function () {
    const windows = await electron.app.windows();
    const window = await electron.window;
    await window.screenshot({
      path: 'test-results/screenshots/launch/starts-the-menubar-window.png',
      fullPage: true,
    });
    expect(await window.title()).toEqual('GitDock');
  });
  test('can log in', async function () {
    const window = await electron.window;
    await window.click('#instance-checkbox');
    window.$eval(
      '#access-token-input',
      (el, token) => (el.value = token),
      process.env.ACCESS_TOKEN,
    );
    window.$eval('#instance-url-input', (el) => (el.value = 'https://gitlab.com'));
    await window.click('#login-instance-button');
    await window.waitForNavigation();

    const issues = window.locator('#issues-count');
    const mrs = window.locator('#mrs-count');
    const todos = window.locator('#todos-count');

    await window.screenshot({
      path: 'test-results/screenshots/launch/can-log-in.png',
      fullPage: true,
    });
    expect(await issues.count()).toBe(1);
    expect(await mrs.count()).toBe(1);
    expect(await todos.count()).toBe(1);
  });
});

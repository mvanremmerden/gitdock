const { test, expect } = require('@playwright/test');
const { newApp } = require('./util');

test.describe('Themes', function () {
  const getBackgroundColor = async (page) => {
    const body = await page.waitForSelector('body');
    return await body.evaluate((button) => getComputedStyle(button).backgroundColor);
  };

  test.describe('default theme', () => {
    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp();
      window = electron.window;
    });

    test('has the correct background color', async function () {
      const color = await getBackgroundColor(window);
      await window.screenshot({
        path: 'test-results/screenshots/themes/has-the-correct-background-color-1.png',
        fullPage: true,
      });
      expect(color).toEqual('rgb(9, 12, 16)');
    });
  });

  test.describe('dark theme', () => {
    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp({ theme: 'dark' });
      window = electron.window;
    });

    test('has dark background color', async function () {
      const color = await getBackgroundColor(window);
      await window.screenshot({
        path: 'test-results/screenshots/themes/has-the-correct-background-color-2.png',
        fullPage: true,
      });
      expect(color).toEqual('rgb(9, 12, 16)');
    });
  });

  test.describe('light theme', () => {
    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp({ theme: 'light' });
      window = electron.window;
    });

    test('has light background color', async function () {
      const color = await getBackgroundColor(window);
      await window.screenshot({
        path: 'test-results/screenshots/themes/has-the-correct-background-color-3.png',
        fullPage: true,
      });
      expect(color).toEqual('rgb(255, 255, 255)');
    });
  });
});
